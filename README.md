# Тестовое задание
Необходимо разработать компонент для выбора узла из каталога. 
Каталог представляет собой иерархическую структуру, в которой первый уровень всегда Группа и его дочерние элементы - конечные узлы. Подробное описание структуры можно найти в файле `types/types.js`.  

#Получение каталога

Перед получением каталога, необходимо авторизоваться в системе. После авторизации можно вызывать метод получения каталога, передав в заголовке `shop-auth-token` токен, полученный при авторизации. 

Доступные методы:
- `POST http://shop.apgrup.ru/v1/login` - авторизация в системе. Не требует дополнительных параметров. 
- `GET http://shop.apgrup.ru/v1/partnames/existing` - получение каталога наименований.
 
## Логика работы

Компонент представляет собой блок, в котором выводится выбранный на данный момент узел.

[![](assets/1-small.png)](assets/1.png)

При клике на блок открывается попап, в котором отображаются все доступные группы.

[![](assets/2-small.png)](assets/2.png)

При клике на группу раскрывается список всех доступных наименований.

[![](assets/3-small.png)](assets/3.png)

При вводе в строку поиска происходит фильтрация групп и элементов по полю `title`. В этом режиме должны быть раскрыты все группы.

[![](assets/4-small.png)](assets/4.png)

При клике по наименованию попап должен закрываться и отображать выбранный узел

[![](assets/5-small.png)](assets/5.png)

## Основные требования:
- При выборе наименования, компонент должен отдавать родителю выбранную группу и наименование через колбэк 
- Текущая выбранная группа и наименование должны приходить в компонент от родителя
- Поиск без учета регистра
- Наименования необходимо расположить в 2 столбца таким образом, чтобы наименования во второй колонке продолжали алфавитный порядок первой. [Пример](/assets/3.png)
- Попап должен отображаться на весь экран, при нахождении компонента внутри контейнера с `overflow: hidden`

## Результат
В результате нужно предоставить репозиторий, в котором будет исходный код компонента и скрипт для сборки. 

Инструмент для сборки можно выбрать любой. В примере используется [Parcel](https://parceljs.org/)