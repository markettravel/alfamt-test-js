type PartName = {
  id: number,
  title: string,
  titleTranslit: string,
};

type PartGroup = {
  id: number,
  title: string,
  titleTranslit: string,
  partNames: Array<PartName>,
};